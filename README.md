# Data Cache Prefetch Simulation

The project contains implementations of three types of techniques used in data cache prefetching (all written in single file prefetch_example.cpp) :
* Next-N-Line prefetching
* Stride prefetching
* Distance prefetching

All techniques use the same cache (dcache_for_prefetcher.hpp) with least-recently-used (LRU) replacement policy.

## Report

A simple report containing analysis for the techniques over the testing output (dir tests) and personal choice of prefetcher can be found in 'report.pdf'.

### Simulation handler

The branch predictors testing was executed on INTEL PIN TOOL:
pin-3.5-97503-gac534ca30-gcc-linux


## Authors

* **Boyan Yotov** - *Full work* - [wolfstrasz](https://gitlab.com/wolfstrasz)

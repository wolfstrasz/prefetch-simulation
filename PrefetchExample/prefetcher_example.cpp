#include "pin.H"

#include <stdlib.h>

#include "dcache_for_prefetcher.hpp"
#include "pin_profile.H"

// Simulation will be stop once this number of instructions is executed
#define STOP_INSTR_NUM 1000000000 //1b instrs
// Simulator heartbeat rate
#define SIMULATOR_HEARTBEAT_INSTR_NUM 100000000 //100m instrs

ofstream outFile;
Cache *cache;
UINT64 loads;
UINT64 stores;
UINT64 hits;
UINT64 cacheHits;
UINT64 prefHits;
UINT64 accesses, prefetches;
bool use_next_n_lines;
bool use_stride_prefetcher;
bool use_distance_prefetcher;
int sets;
int associativity;
int blockSize;
UINT64 iCount = 0;


/* ===================================================================== */
/* Commandline Switches */
/* ===================================================================== */

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE,    "pintool",
    "o", "data", "specify output file name");
KNOB<string> KnobPrefetcherType(KNOB_MODE_WRITEONCE, "pintool",
    "pref_type", "none", "specify type of prefetcher to be used");
KNOB<UINT32> KnobAggression(KNOB_MODE_WRITEONCE , "pintool",
    "aggr", "2", "the aggression of the prefetcher");
KNOB<UINT32> KnobCacheSets(KNOB_MODE_WRITEONCE, "pintool",
    "sets","64", "number of sets in the cache");
KNOB<UINT32> KnobLineSize(KNOB_MODE_WRITEONCE, "pintool",
    "b","4", "cache block size in bytes");
KNOB<UINT32> KnobAssociativity(KNOB_MODE_WRITEONCE, "pintool",
    "a","2", "cache associativity (1 for direct mapped)");


/* ===================================================================== */

// Print a message explaining all options if invalid options are given
INT32 Usage()
{
  cerr << "This tool represents a cache simulator." << endl;
  cerr << KNOB_BASE::StringKnobSummary() << endl;
  return -1;
}

// Count the instructions
VOID docount()
{
  // Update instruction counter
  iCount++;
  // Print this message every SIMULATOR_HEARTBEAT_INSTR_NUM executed
  if (iCount % SIMULATOR_HEARTBEAT_INSTR_NUM == 0) {
    std::cerr << "Executed " << iCount << " instructions." << endl;
  }
  // Release control of application if STOP_INSTR_NUM instructions have been executed
  if (iCount == STOP_INSTR_NUM) {
    PIN_Detach();
  }
}

// Take checkpoints with stats in the output file
VOID TerminateSimulationHandler(VOID *v)
{
  outFile << "Accesses: " << accesses << " Loads: "<< loads << " Stores: " << stores <<   endl;
  outFile << "Hits: " << hits << endl;
  if (accesses > 0) outFile << "Hit rate: " << double(hits) / double(accesses) << endl;
  outFile << "Prefetches: " << prefetches << endl;
  outFile << "Prefetch hits: " << cache->getPrefHits() << endl;
  outFile << "Successful prefetches: " << cache->getSuccessfulPrefs() << endl;
  outFile.close();
  // Following outputs will be visible if the porgram detaches, but not if it terminates
  std::cout << endl << "PIN has been detached at iCount = " << iCount << endl;
  std::cout << endl << "Simulation has reached its target point. Terminate simulation." << endl;
  if (accesses > 0) std::cout << "Cache Hit Rate:\t" << double(hits) / double(accesses) << endl;
  std::exit(EXIT_SUCCESS);
}

/* ===================================================================== */

/* Next Line Prefetcher
    This is an example implementation of the next line Prefetcher.
    The stride prefetcher would also need to take as argument the program counter
    of the load instruction.
*/
void prefetchNextNLines(UINT64 addr)
{
  for (int i = 1; i <= aggression; i++) {
    UINT64 nextAddr = addr + (i * blockSize) ;
    if (!cache->exists(nextAddr)) {  // Use the member function Cache::exists(UINT64) to query whether
                                     //  a block exists in the cache w/o triggering any LRU changes (not after a demand access)
      cache->prefetchFillLine(nextAddr); // Use the member function Cache::prefetchFillLine(UINT64) when you fill the cache in the LRU way for prefetch accesses
      prefetches++;
    }
  }
}

/* ===================================================================== */

//---------------------------------------------------------------------
//##############################################
/*
 * Your changes here.
 *
 * Put your prefetcher implementation here
 *
 * You can modify the functions Load() and Store() where necessary to implement your functionality
 *
 * DIRECTIONS ON USING THE COMMAND LINE ARGUMENT
 *  Use the command line argument (-pref_type) to denote which prefetcher must be created choices are: none, stride, distance, next_n_lines (default is 'none')
 *  --Example: pin -t (tool) -pref_type stride -- (benchmark)
 *  Use the command line argument (-o) to specify any name for your output file, if you don't a default name will be used
 *  Use the command line argument(-aggr) to specify the aggressiveness of the prefetcher
 *  --The integer variable "aggression" indicates the aggressiveness indicated by the command line argument (-aggr)
 *
 * DATA CACHE:
 *  The variable "cache" points to a Cache object that implements a 2-way set associative cache with 64 sets and block size = 4 bytes.
 *
 * STATS:
 * ***You MUST take care to properly increment the counters "prefetches", "accesses" and "hits". ("accesses" and "hits" are already incremented properly)
 *    The integer variable "prefetches" should count the number of prefetched blocks.
 *    The integer variable "accesses" counts the number of memory accesses performed by the program.
 *    The integer variable "hits" counts the number of memory accesses that actually hit in the data cache.
 *  The cache also maintains stats for prefetching:
 *    The member function Cache::getPrefHits() returns how many times prefetched blocks have been accessed.
 *    The member function Cache::getSuccessfulPrefs() returns how many of the prefetched block into the cache were actually used.
 *  Counters "prefHits" and "cacheHits" are only meant to help you and you can avoid using them if you don't want them.
 *
 *
 */
//##############################################
//---------------------------------------------------------------------
typedef UINT64 STATE;
#define MAX_ENTRIES 64

// FSM State assignment for Stride RPT States
#define INITIAL			 0
#define STEADY			 1
#define TRANSIENT		 2
#define NO_PREDICTION	 3

//###################################################
// Stride RPT Entry
//###################################################
class Stride_RPT_Entry
{
public:
    // Constructor
	Stride_RPT_Entry(){
        tag = 0;
        prev_addr = 0;
        stride = 0;
        state = INITIAL;
    }
    //Destructor. No pointers to delete.
	~Stride_RPT_Entry() {};

	ADDRINT tag;
	ADDRINT prev_addr;
	UINT64 stride;
	STATE state;
};


//##########################################################
// Stride Prefetcher Class
//##########################################################
class StridePrefetcher
{
    Stride_RPT_Entry *rpt;                              // Reference Prediction Table
    Stride_RPT_Entry *currEntry;                        // Current entry to edit/update
    UINT64           num_entries_used;                  // After 64 of them used -> use random replacement

    void addEntry(ADDRINT pc, ADDRINT addr);            // Add a new entry at position currEntry
	bool findEntry(ADDRINT pc);                         // Returns true if entry is found and sets currEntry to its address.
	ADDRINT prefetch(ADDRINT addr);                     // Prefetches blocks and returns the address of last prefetched block.

public:
	StridePrefetcher();
    ~StridePrefetcher();
	void access(ADDRINT pc, ADDRINT addr);              // Requests an access to an entry in the RPT
};

// Constructor
StridePrefetcher::StridePrefetcher(){
    rpt              = new Stride_RPT_Entry[MAX_ENTRIES]();
    num_entries_used = 0;
    currEntry        = NULL;
}

// Destructor
StridePrefetcher::~StridePrefetcher(){
    delete rpt;
    delete currEntry;
}

// Prefetch blocks to cache
ADDRINT StridePrefetcher::prefetch(ADDRINT addr) {
	UINT64 nextAddr = 0;
	for (int i = 1; i <= aggression; i++) {
		nextAddr = addr + (i * currEntry->stride);
		if (!cache->exists(nextAddr)) {
			cache->prefetchFillLine(nextAddr);
			prefetches++;
		}
	}
	return nextAddr;
}

// Search for Entry in the RPT with tag = pc
bool StridePrefetcher::findEntry(ADDRINT pc) {
	for (UINT64 i=0; i < num_entries_used; i++)
		if (pc == rpt[i].tag) {
            currEntry = &rpt[i];
            return true;
        }
	return false;
}

// Creates a new entry or replaces an old one at position pointed by currEntry
void StridePrefetcher::addEntry(ADDRINT pc, ADDRINT addr){
    currEntry->tag = pc;
    currEntry->prev_addr = addr;
    currEntry->stride = 0;
    currEntry->state = INITIAL;
}

// Access an entry to update
void StridePrefetcher::access(ADDRINT addr, ADDRINT pc) {
    // Search for an existing entry to update
	if (findEntry(pc)) {
		bool correct = (currEntry->prev_addr + currEntry->stride == addr) ? true : false;

        /* The state to state transitions following Jean-Loup Baer's
            and Tien-Fu Chen's on-chip preloading scheme  */

		// Prefetch (then keep staying at STEADY)
		if (correct && currEntry->state == STEADY) {
			currEntry->prev_addr = prefetch(addr);
		}

		// Transition
		else if (!correct && currEntry->state == INITIAL) {
			currEntry->stride = addr - currEntry->prev_addr;
			currEntry->prev_addr = addr;
			currEntry->state = TRANSIENT;
		}

		// Moving to Steady State
		else if (correct && (currEntry->state == INITIAL || currEntry->state == TRANSIENT)) {
			currEntry->prev_addr = addr;
			currEntry->state = STEADY;
		}

		// Steady State is over
		else if (!correct && currEntry->state == STEADY) {
			currEntry->prev_addr = addr;
			currEntry->state = INITIAL;
		}

		// Detection of irregular pattern
		else if (!correct && currEntry->state == TRANSIENT) {
			currEntry->stride = addr - currEntry->prev_addr;
			currEntry->prev_addr = addr;
			currEntry->state = NO_PREDICTION;
		}

		// No Prediction State is over
		else if (correct && currEntry->state == NO_PREDICTION) {
			currEntry->prev_addr = addr;
			currEntry->state = TRANSIENT;
		}

		// Irregular pattern (keep staying at NO_PREDICTION)
		else if (!correct && currEntry->state == NO_PREDICTION) {
			currEntry->stride = addr - currEntry->prev_addr;
			currEntry->prev_addr = addr;
		}
	}
    // Entry was not found in RPT, put new entry following RPT replacement policy
	else {
        // Replacement policy
        currEntry = (num_entries_used < MAX_ENTRIES) ? &rpt[++num_entries_used] : &rpt[rand()%MAX_ENTRIES];
        addEntry(pc, addr);
	}
}

/* Replacement Policy explanation:
  If num_entries_used < MAX_ENTRIES -> there still unused entries so we use
  rpt[num_entries], which is the next free entry, then increment num_entries.
  Else -> Choose a random entry from all 64 (MAX_ENTRIES)                       */

//##########################################################
// Distance RPT Entry Class
//##########################################################
class Distance_RPT_Entry
{
public:
	Distance_RPT_Entry();
	~Distance_RPT_Entry();

	int   tag;
	int   *predicted_distances;
    UINT64 num_distances_used;
};

// Constructor
Distance_RPT_Entry::Distance_RPT_Entry() {
	tag                 = 0;                        // Tag of corresponding entry
	predicted_distances = new int[aggression];      // Reference history (correlation table)
	num_distances_used  = 0;                        // Number of entries in predicted_distances
}

// Destructor
Distance_RPT_Entry::~Distance_RPT_Entry(){
	delete predicted_distances;
}

//##########################################################
// Distance Prefetcher Class
//##########################################################

class DistancePrefetcher
{
    bool	           initialized;                 // Check for first miss
    Distance_RPT_Entry *rpt;                        // Reference Prediction Table
    Distance_RPT_Entry *currEntry;                  // Pointer to entry that will be edited
    UINT64	           num_entries_used;            // Entries used in rpt
    UINT64             prev_miss_address;
    long	           prev_distance;

    bool findEntry(int tag);                       // Find an entry with the tag and sets currEntry to it
    void prefetch(ADDRINT addr);                    // Prefeches blocks
    void trainEntry(int distance);                 // Trains the currEntry with distance
    void addEntry(int tag);                        // Creates a new entry at position pointed by currEntry

public:
	DistancePrefetcher();
	~DistancePrefetcher();

	void access(ADDRINT addr);
};

// Constructor
DistancePrefetcher::DistancePrefetcher(){
	rpt               = new Distance_RPT_Entry[MAX_ENTRIES]();
	num_entries_used  = 0;
    prev_miss_address = 0;
	prev_distance     = 0;
    currEntry         = NULL;
    initialized       = false;
}

// Destructor
DistancePrefetcher::~DistancePrefetcher() {
	delete rpt;
	delete currEntry;
}

// Trains the entry pointed by currEntry
void DistancePrefetcher::trainEntry(int distance) {
    UINT64 index = (currEntry->num_distances_used < aggression) ?
                    (++(currEntry->num_distances_used)) : (rand() % aggression); // Entry replacement policy
	currEntry->predicted_distances[index] = distance;                            // Put the new distance
}

// Prefetches blocks
void DistancePrefetcher::prefetch(ADDRINT addr) {
	UINT64 nextAddr;
	for (UINT64 i = 0; i < currEntry->num_distances_used; i++) {
		nextAddr = addr + currEntry->predicted_distances[i];
		if (!cache->exists(nextAddr)) {
			cache->prefetchFillLine(nextAddr);
			prefetches++;
		}
	}
}

// Find entry with a given distance tag
bool DistancePrefetcher::findEntry(int tag) {
	for (UINT64 i = 0; i < num_entries_used; i++)
		if (tag == rpt[i].tag) {
			currEntry = &rpt[i];
			return true;
		}
	return false;
}

// Adds a new entry or replaces an old one where tag = distance
void DistancePrefetcher::addEntry(int tag){
    currEntry->tag = tag;
    currEntry->num_distances_used = 0;
}

void DistancePrefetcher::access(ADDRINT addr) {
    // On first global miss i.e no pair(miss,miss) present
	if (!initialized) {
		prev_miss_address = addr;
		initialized = true;
	}

    // On second miss Create an entry with tag = distance
	else if (num_entries_used == 0) {
        int distance      = addr - prev_miss_address;
		currEntry         = &rpt[++num_entries_used];
        addEntry(distance);

        // Set previous values
        prev_distance     = addr - prev_miss_address;
		prev_miss_address = addr;
	}

    // On every next miss
	else {
		int distance = addr - prev_miss_address;
        // Search for an entry to prefetch addresses
		if (findEntry(distance)) {
            prefetch(addr);
		} else {
        // Add new entry following RPT replacement policy
            currEntry = (num_entries_used < MAX_ENTRIES) ?
                         &rpt[++num_entries_used] : &rpt[rand()%MAX_ENTRIES];
            addEntry(distance);
		}

        // Search for an entry to train. If it was found => train it.
		if(findEntry(prev_distance)){
			trainEntry(distance);
        }

		// Set previous values
		prev_miss_address = addr;
		prev_distance = distance;
	}
}

// Prefetchers initialization.
StridePrefetcher* stridePrefetcher;
DistancePrefetcher* distancePrefetcher;

/* ===================================================================== */

/* Action taken on a load. Load takes 2 arguments:
    addr: the address of the demanded block (in bytes)
    pc: the program counter of the load instruction
*/
void Load(ADDRINT addr, ADDRINT pc)
{
  accesses++;
  loads++;
  if (cache->probeTag(addr)) { // Use the function Cache::probeTag(UINT64) wehen you probe the cache after a demand access
    hits++;
  }
  else {
	  cache->fillLine(addr); // Use the member function Cache::fillLine(addr) when you fill in the MRU way for demand accesses
	  if (use_next_n_lines) prefetchNextNLines(addr); // Call the Next line prefetcher on a load miss
	  if (use_stride_prefetcher) stridePrefetcher->access(addr, pc); // Call the Stride Prefetcher on a load miss
	  if (use_distance_prefetcher) distancePrefetcher->access(addr); // Call the Distance Prefetcher on a miss
  }
}

/* ===================================================================== */

//Action taken on a store
void Store(ADDRINT addr, ADDRINT pc)
{
  accesses++;
  stores++;
  if (cache->probeTag(addr)) hits++;
  else  {
      cache->fillLine(addr);
      if (use_distance_prefetcher) distancePrefetcher->access(addr); // Call the Distance Prefetcher on a miss
  }
}

/* ===================================================================== */

// Receives all instructions and takes action if the instruction is a load or a store
// DO NOT MODIFY THIS FUNCTION
void Instruction(INS ins, void * v)
{
  //Insert a call before every instruction to count them
  INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)docount, IARG_END);

 // Insert a call for every Load and Store
  if (INS_IsMemoryRead(ins) && INS_IsStandardMemop(ins)) {
    INS_InsertPredicatedCall(
        ins, IPOINT_BEFORE, (AFUNPTR) Load,
        (IARG_MEMORYREAD_EA), IARG_INST_PTR, IARG_END);
 }
  if ( INS_IsMemoryWrite(ins) && INS_IsStandardMemop(ins))
  {
    INS_InsertPredicatedCall(
      ins, IPOINT_BEFORE,  (AFUNPTR) Store,
      (IARG_MEMORYWRITE_EA), IARG_INST_PTR, IARG_END);
  }
}

/* ===================================================================== */

// Gets called when the program finishes execution
void Fini(int code, VOID * v)
{
  TerminateSimulationHandler(v);
}

/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */

int main(int argc, char *argv[])
{
    PIN_InitSymbols();
    if( PIN_Init(argc,argv) )
    {
        return Usage();
    }

    // Initialize stats
    hits        = 0;
    accesses    = 0;
    prefetches  = 0;
    cacheHits   = 0;
    prefHits    = 0;
    loads       = 0;
    stores      = 0;

    // Set use of prefetchers to false
    use_next_n_lines        = false;
    use_stride_prefetcher   = false;
    use_distance_prefetcher = false;

    // Get command line values
    aggression      = KnobAggression.Value();
    sets            = KnobCacheSets.Value();
    associativity   = KnobAssociativity.Value();
    blockSize       =  KnobLineSize.Value();

    // Choose prefetcher to be used
    if (KnobPrefetcherType.Value() == "none") {
  	  std::cerr << "Not using any prefetcher" << std::endl;
    }
    else if (KnobPrefetcherType.Value() == "next_n_lines") {
  	  std::cerr << "Using Next-N-Lines Prefetcher."<< std::endl;
      use_next_n_lines = true;
    }
    else if (KnobPrefetcherType.Value() == "stride") {
  	  std::cerr << "Using Stride Prefetcher." << std::endl;
      use_stride_prefetcher = true;
      stridePrefetcher = new StridePrefetcher();
    }
    else if (KnobPrefetcherType.Value() == "distance") {
  	  std::cerr << "Using Distance Prefetcher." << std::endl;
      use_distance_prefetcher = true;
      distancePrefetcher = new DistancePrefetcher();
    }
    else {
      std::cerr << "Error: No such type of prefetcher. Simulation will be terminated." << std::endl;
      std::exit(EXIT_FAILURE);
    }

   // create a data cache
    cache = new Cache(sets, associativity, blockSize);

    // create an output file with an appropriate name
    string outName(KnobOutputFile.Value());
    outFile.open(outName.c_str());

    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);
    PIN_AddDetachFunction(TerminateSimulationHandler, 0);

    // Never returns
    PIN_StartProgram();

    return 0;
}

/* ===================================================================== */
/* eof */
/* ===================================================================== */
